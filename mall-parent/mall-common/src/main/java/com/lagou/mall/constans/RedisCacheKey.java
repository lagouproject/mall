package com.lagou.mall.constans;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/6
 * @description
 */
public class RedisCacheKey {
    /**
     * 缓存key前缀
     */
    public static final String KEY_PREFIX = "goodscenter:";
    /**
     * 商品列表key
     */
    public static final String GOODS_LIST_KEY = "gl";
    /**
     * 商品详情key
     */
    public static final String GOODS_DETAIL_KEY = "gd";
    /**
     * 分割符号
     */
    public static final String SPLIT = ":";
}
