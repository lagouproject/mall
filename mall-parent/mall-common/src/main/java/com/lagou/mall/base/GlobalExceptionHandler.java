package com.lagou.mall.base;


import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/1
 * @slogan 天下风云出我辈，一入代码岁月摧
 * @description 全局异常捕获
 */
@ControllerAdvice(basePackages = "com.mayikt.api.impl")
public class GlobalExceptionHandler {
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public Map<String, Object> errorResult() {
        Map<String, Object> errorResultMap = new HashMap<String, Object>();
        errorResultMap.put("code", "500");
        errorResultMap.put("msg", "系统出现错误!");
        return errorResultMap;
    }
}
