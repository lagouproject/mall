package com.lagou.mall.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/1
 * @description
 */
@Component
public class RedisUtil {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 存放string类型
     *
     * @param key     key
     * @param data    数据
     * @param timeout 超时间
     */
    public boolean setExpireKey(String key, String data, Long timeout) {
        try {
            stringRedisTemplate.opsForValue().set(key, data);
            if (timeout != null) {
                stringRedisTemplate.expire(key, timeout, TimeUnit.SECONDS);
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 存放string类型
     *
     * @param key  key
     * @param data 数据
     */
    public boolean set(String key, String data) {
        return setExpireKey(key, data, null);
    }

    /**
     * 根据key查询string类型
     *
     * @param key
     * @return
     */
    public String get(String key) {
        String value = null;
        try {
             value = stringRedisTemplate.opsForValue().get(key);
        }catch (Exception e){
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 设置hash值
     *
     * @param key     外部key
     * @param item    内部key
     * @param value   值
     * @param timeout 过期时间
     */
    public boolean hset(String key, String item, Object value, long timeout) {
        try {
            stringRedisTemplate.opsForHash().put(key, item, value);
            if (timeout > 0L) {
                this.expire(key, timeout);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 过期清除key
     * @param key key
     * @param timeout 过期时间
     */
    public void expire(String key, long timeout) {
        try {
            stringRedisTemplate.expire(key, timeout, TimeUnit.SECONDS);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @param key
     * @param map
     */
    public boolean hmset(String key, Map<String, Object> map) {
        try {
            stringRedisTemplate.opsForHash().putAll(key, map);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 获取hash值
     * @param key 外部key
     * @param item 内部key
     * @return
     */
    public Object hget(String key,String item){
        try {
            return stringRedisTemplate.opsForHash().get(key, item);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * zset添加元素
     * @param key key
     * @param value value
     * @param score 分数
     * @return
     */
    public boolean zAdd(String key,String value,Double score){
        return stringRedisTemplate.opsForZSet().add(key,value,score);
    }

    /**
     * 移除元素
     * @param key
     * @param values
     * @return
     */
    public long remove(String key,String...values){
        return stringRedisTemplate.opsForZSet().remove(key,values);
    }
    /**
     * 根据对应的key删除key
     *
     * @param keys
     */
    public boolean delKey(String... keys) {
        try {
            if(keys.length == 1){
                stringRedisTemplate.delete(keys[0]);
            }else{
                stringRedisTemplate.delete(CollectionUtils.arrayToList(keys));
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
