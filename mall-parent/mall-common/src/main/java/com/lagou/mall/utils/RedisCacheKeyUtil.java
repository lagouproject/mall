package com.lagou.mall.utils;


import com.lagou.mall.constans.RedisCacheKey;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/6
 * @slogan 天下风云出我辈，一入代码岁月摧
 * @description 缓存key
 */
public class RedisCacheKeyUtil {

    private static final String SPLIT = ":";
    /**
     * 获取商品列表缓存key
     * @return
     */
    public static String getGoodsListKey(){
        return RedisCacheKey.GOODS_LIST_KEY;
    }

    /**
     * 获取商品详情缓存key
     * @param skuId
     * @return
     */
    public static String getGoodsDetalKey(String skuId){
        StringBuilder sb  =new StringBuilder();
        String goodsDetailCacheKey = sb.append(RedisCacheKey.GOODS_LIST_KEY)
                .append(SPLIT)
                .append(skuId)
                .toString();
        return goodsDetailCacheKey;
    }

    /**
     * 批量获取商品缓存key
     * @param skuIds
     * @return
     */
    public static List<String> getGoodsCacheKeyListBySkuIds(List<String> skuIds){
        return skuIds.stream().map(skuId -> {
            StringBuilder sb  =new StringBuilder();
            return sb.append(RedisCacheKey.GOODS_DETAIL_KEY)
                    .append(SPLIT)
                    .append(skuId)
                    .toString();
        }).collect(Collectors.toList());
    }
}
