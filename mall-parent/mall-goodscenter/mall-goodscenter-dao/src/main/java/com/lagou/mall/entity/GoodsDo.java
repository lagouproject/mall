package com.lagou.mall.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/1
 * @description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_goods_sku")
public class GoodsDo {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商品sku编号
     */
    private String skuCode;
    /**
     * 商品spu编号
     */
    private String productCode;
    /**
     * 商品价格
     */
    private BigDecimal salePrice;
    /**
     * 商品图片
     */
    private String skuPictureUrl;
    /**
     * 商品排序
     */
    private Integer skuSort;
    /**
     * 商品描述
     */
    private String notes;
    /**
     * 版本号
     */
    private Integer version;
    /**
     * 商品状态 0不可用  1可用
     */
    private Integer state;
    /**
     * 是否删除   0未删除   1已删除
     */
    @TableLogic(value = "0", delval = "1")
    private Integer deleted;
    /**
     * 修改时间
     */
    private Date modifyTime;
    /**
     * 创建时间
     */
    private Date createTime;

}
