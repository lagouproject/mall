package com.lagou.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lagou.mall.entity.GoodsDo;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/6
 * @slogan 天下风云出我辈，一入代码岁月摧
 * @description
 */
public interface GoodsSkuDaoService extends IService<GoodsDo> {
}
