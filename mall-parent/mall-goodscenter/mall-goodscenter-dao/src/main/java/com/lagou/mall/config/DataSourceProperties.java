package com.lagou.mall.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/14
 * @description
 */
@Data
@ConfigurationProperties(prefix = "spring.datasource")
public class DataSourceProperties {
    private String url;
    private String driverClassName;
    private String username;
    private String password;
}
