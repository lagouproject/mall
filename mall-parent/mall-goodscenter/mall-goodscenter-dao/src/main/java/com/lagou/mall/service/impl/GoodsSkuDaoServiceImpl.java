package com.lagou.mall.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lagou.mall.entity.GoodsDo;
import com.lagou.mall.mapper.GoodsSkuMapper;
import com.lagou.mall.service.GoodsSkuDaoService;
import org.springframework.stereotype.Service;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/6
 * @description
 */
@Service
public class GoodsSkuDaoServiceImpl extends ServiceImpl<GoodsSkuMapper, GoodsDo> implements GoodsSkuDaoService {
}
