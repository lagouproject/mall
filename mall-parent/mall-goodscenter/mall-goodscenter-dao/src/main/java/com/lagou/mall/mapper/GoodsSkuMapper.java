package com.lagou.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lagou.mall.entity.GoodsDo;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/6
 * @description
 */
public interface GoodsSkuMapper extends BaseMapper<GoodsDo> {
}
