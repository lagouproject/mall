package com.lagou.mall.api;


import com.lagou.mall.dto.GoodsDto;

import java.util.List;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/6
 * @description 商品刷新缓存api
 */
public interface IGoodsCacheService {
    /**
     * 刷新商品缓存
     *
     * @param list
     */
    void refreshCache(List<GoodsDto> list);

}
