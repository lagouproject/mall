package com.lagou.mall.api;

import com.lagou.mall.base.Result;
import com.lagou.mall.dto.GoodsDto;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/6
 * @description 商品服务api
 */
public interface IGoodsService {
    /**
     * 查询商品详情
     * @param skuCode
     * @return
     */
    Result<GoodsDto> getGoodsDetail(Long skuCode);
}
