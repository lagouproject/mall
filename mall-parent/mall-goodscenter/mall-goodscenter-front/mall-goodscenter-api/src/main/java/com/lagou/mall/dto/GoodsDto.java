package com.lagou.mall.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/6
 * @description
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GoodsDto {
    private Integer id;
    private String skuCode;
    private String productCode;
    private BigDecimal saleprice;
    private String skuPictureUrl;
    private Integer skuSort;
    private String notes;
    private Integer state;
    private Integer deleted;
    private Integer version;
    private Date modifyTime;
    private Date createTime;
}
