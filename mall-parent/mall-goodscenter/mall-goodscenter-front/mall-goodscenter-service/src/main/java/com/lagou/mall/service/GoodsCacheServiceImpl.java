package com.lagou.mall.service;

import com.lagou.mall.api.IGoodsCacheService;
import com.lagou.mall.dto.GoodsDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/9
 * @description 商品缓存服务类
 */
@Slf4j
@Service("goodsCacheService")
public class GoodsCacheServiceImpl implements IGoodsCacheService {


    @Override
    public void refreshCache(List<GoodsDto> list) {
        return;
    }
}
