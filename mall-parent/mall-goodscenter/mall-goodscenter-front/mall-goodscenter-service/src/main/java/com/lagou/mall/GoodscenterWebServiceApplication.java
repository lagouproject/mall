package com.lagou.mall;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/23
 * @slogan 天下风云出我辈，一入代码岁月摧
 * @description
 */
@Slf4j
@SpringBootApplication
@MapperScan("com.lagou.mall.mapper")
public class GoodscenterWebServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoodscenterWebServiceApplication.class,args);
    }
}
