package com.lagou.mall.service;

import com.lagou.mall.api.IGoodsService;
import com.lagou.mall.base.Result;
import com.lagou.mall.dto.GoodsDto;
import com.lagou.mall.entity.GoodsDo;
import com.lagou.mall.utils.BeanCopyUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/9
 * @description 商品服务
 */
@Slf4j
@Service("goodsService")
public class GoodsServiceImpl implements IGoodsService {

    @Autowired
    private GoodsSkuDaoService goodsSkuDaoService;

    @Override
    public Result<GoodsDto> getGoodsDetail(Long skuCode) {
        List<GoodsDo> list = goodsSkuDaoService.lambdaQuery().ge(GoodsDo::getSkuCode, skuCode).list();
        GoodsDto goodsDto = BeanCopyUtil.doToDto(list.get(0), GoodsDto.class);
        return Result.success(goodsDto);
    }
}
