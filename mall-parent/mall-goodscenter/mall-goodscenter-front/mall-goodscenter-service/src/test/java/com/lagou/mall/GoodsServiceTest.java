package com.lagou.mall;

import com.lagou.mall.entity.GoodsDo;
import com.lagou.mall.service.GoodsSkuDaoService;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author jizy
 * @version 1.0.0
 * @create 2021/9/14
 * @slogan 天下风云出我辈，一入代码岁月摧
 * @description
 */
@Slf4j
@SpringBootTest(classes = GoodscenterWebServiceApplication.class)
@RunWith(SpringRunner.class)
//@ActiveProfiles("dev")
public class GoodsServiceTest {

    @Autowired
    private GoodsSkuDaoService goodsSkuDaoService;

    @Value("${spring.datasource.url}")
    private String url;

    /**
     * 查询商品列表信息
     * @return 返回商品列表
     */
    @Test
    public void getGoodsList(){
        List<GoodsDo> list = goodsSkuDaoService.lambdaQuery().list();

        log.info("查询出来的数据是:{}", JSONObject.toJSONString(list));
        System.out.println(JSONObject.toJSONString(list));
    }
}
